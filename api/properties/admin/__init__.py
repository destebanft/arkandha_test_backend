from django.contrib import admin

from properties.admin.property import PropertyAdmin
from properties.models.property import Property
from properties.admin.property_relationship import PropertyRelationShipAdmin
from properties.models.property_relationship import PropertyRelationShip


admin.site.register(Property, PropertyAdmin)
admin.site.register(PropertyRelationShip, PropertyRelationShipAdmin)