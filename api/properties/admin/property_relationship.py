from django.contrib import admin

from properties.models.property_relationship import PropertyRelationShip

class PropertyRelationShipAdmin(admin.ModelAdmin):
	model = PropertyRelationShip