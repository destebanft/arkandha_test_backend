from django.contrib import admin

from properties.models.property import Property

class PropertyAdmin(admin.ModelAdmin):
	model = Property