from django.db import models


class PropertyRelationShip(models.Model):
	id = models.BigAutoField(
  	primary_key=True
  )
	owner = models.ForeignKey(
		'owners.Owner',
		on_delete=models.CASCADE
	)
	property = models.ForeignKey(
		'properties.Property',
		on_delete=models.CASCADE
	)
  
	class Meta:
		unique_together = ['owner', 'property']
  
	def __str__(self):
		return f'{self.owner.name} property of {self.property.address}'

  
