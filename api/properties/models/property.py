from django.db import models
from django.utils.translation import gettext as _

from properties.models.property_relationship import PropertyRelationShip

class Property(models.Model):

	class Type(models.IntegerChoices):
		RURAL = 0, _('Rural')
		URBAN = 1, _('Urban')

	id = models.BigAutoField(
  	primary_key=True
  )
	address = models.CharField(
		max_length=40
	)
	real_estate_number = models.CharField(
		max_length=30,
		unique=True,
		null=True,
	)
	cadastral_id = models.CharField(
		max_length=30,
		null=True,
		blank=True
	)
	type = models.IntegerField(
		choices=Type.choices
	)

	owners = models.ManyToManyField(
		'owners.Owner',
		through=PropertyRelationShip
	)
  
	REQUIRED_FIELDS = ['real_estate_number', 'address', 'type']

	def __str__(self):
		return f'{self.get_type_display()} - {self.address}' 