from rest_framework import serializers

from properties.models.property import Property


class PropertySerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Property
		fields = [
			'id',
			'address',
			'real_estate_number',
			'cadastral_id',
			'type',
			'owners'
		]
