from django.urls import path

from properties.views.property import PropertyView
from properties.views.assign_owners import AssignOwnersView

urlpatterns = [
  path('', PropertyView.as_view()),
  path('assign_owners', AssignOwnersView.as_view())
]