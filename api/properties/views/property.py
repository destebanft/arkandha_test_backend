from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.views import APIView
from django.db import IntegrityError

from owners.models.owner import Owner
from properties.models.property import Property
from properties.serializers.property import PropertySerializer
from utils.response import error_handler, ResponseSuccess, ResponseError

class PropertyView(APIView):

  @error_handler
  def get(self, request, *args, **kwargs):
    address = request.GET.get('address', None)
    real_estate_number = request.GET.get('real_estate_number', None)
    type = request.GET.get('type', None)
    cadastral_id = request.GET.get('cadastral_id', None)
    name_owner = request.GET.get('name_owner', None)
    identification_owner = request.GET.get('identification_owner', None)
    properties = Property.objects.all()
    owners = Owner.objects.all()
    if name_owner:
      owners = owners.filter(name__icontains=name_owner)
      properties = properties.filter(owners__in=owners)
    if identification_owner:
      owners = owners.filter(identification__icontains=name_owner)
      properties = properties.filter(owners__in=owners)
    if type:
      properties = properties.filter(type = type)
    if real_estate_number:
      properties = properties.filter(real_estate_number__icontains = real_estate_number)
    if cadastral_id:
      properties = properties.filter(cadastral_id__icontains = cadastral_id)
    if address:
      properties = properties.filter(address__icontains = address)
    properties_json = PropertySerializer(properties, many=True).data
    return ResponseSuccess(properties_json, status=status.HTTP_200_OK)

  @error_handler
  def post(self, request):
    
    address = request.data.get('address', None)
    real_estate_number = request.data.get('real_estate_number', None)
    cadastral_id = request.data.get('cadastral_id', None)
    type = request.data.get('type', None)

    if not real_estate_number:
      return ResponseError("real_estate_number_missed")

    if not address:
      return ResponseError("address_missed") 

    if not type:
      return ResponseError("type_missed")  
 
    try:
      new_property = Property(
        address=address,
			  real_estate_number=real_estate_number,
			  cadastral_id=cadastral_id,
			  type=type 
      )
      new_property.save()
      return ResponseSuccess('Property added successfully', status=status.HTTP_200_OK)
    except IntegrityError:
      return ResponseError('real_estate_number_already_exists')
    
		

   