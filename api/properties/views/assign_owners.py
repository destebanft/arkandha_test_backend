from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.views import APIView
from django.db import IntegrityError

from owners.models.owner import Owner
from properties.models.property import Property
from properties.models.property_relationship import PropertyRelationShip
from utils.response import error_handler, ResponseSuccess, ResponseError

class AssignOwnersView(APIView):

  @error_handler
  def post(self, request):
    owners = request.data.get('owners', None)
    property = request.data.get('property', None)

    if not owners:
      return ResponseError('owners_missed') 

    if not property:
      return ResponseError('property_missed')  
      
    try:
      property = Property.objects.get(pk=property)
      for owner in owners.split(','):
        owner_model = Owner.objects.get(pk=owner)
        new_relation = PropertyRelationShip(
          owner=owner_model,
          property=property,
        )
        new_relation.save()
      return ResponseSuccess('Owners assign successfullt', status=status.HTTP_200_OK)
    except:
      return ResponseError('error_ocurred')
    

    
		

   