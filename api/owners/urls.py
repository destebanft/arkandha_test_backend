from django.urls import path, include

from owners.views.owner import OwnerView

urlpatterns = [
  path('', OwnerView.as_view())
]