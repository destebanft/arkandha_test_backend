from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.views import APIView
from django.db import IntegrityError

from owners.models.owner import Owner
from owners.serializers.owner import OwnerSerializer
from utils.response import error_handler, ResponseSuccess, ResponseError

class OwnerView(APIView):

  @error_handler
  def get(self, request):
    owners = Owner.objects.all()
    owners_json = OwnerSerializer(owners, many=True).data
    return ResponseSuccess(owners_json, status=status.HTTP_200_OK)

  @error_handler
  def post(self, request):
    name = request.data.get('name', None)
    email = request.data.get('email', None)
    identification = request.data.get('identification', None)

    if not email or not identification:
      return ResponseError("email_or_idenfication_missed") 
    print(name, email, identification)
    try:
      new_owner = Owner(
        name=name,
        email=email,
        identification=identification
      )
      new_owner.save()
      return ResponseSuccess('Owner added successfully', status=status.HTTP_200_OK)
    except IntegrityError:
      return ResponseError('indentification_already_exists')
    
		