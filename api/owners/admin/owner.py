from django.contrib import admin

from owners.models.owner import Owner

class OwnerAdmin(admin.ModelAdmin):
	model = Owner