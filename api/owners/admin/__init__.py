from django.contrib import admin

from owners.admin.owner import OwnerAdmin
from owners.models.owner import Owner

admin.site.register(Owner, OwnerAdmin)
