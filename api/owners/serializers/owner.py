from rest_framework import serializers

from owners.models.owner import Owner


class OwnerSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Owner
		fields = [
			'id',
			'name',
			'email',
			'identification',
		]
