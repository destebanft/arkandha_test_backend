from django.db import models

class Owner(models.Model):
	id = models.BigAutoField(
  	primary_key=True
  )
	name = models.CharField(
		max_length=30
	)
	identification = models.CharField(
		max_length=20,
		unique=True,
		blank=True,
		null=True,
	)
	email = models.EmailField(
		blank=True,
		null=True,
	)
  
	
	REQUIRED_FIELDS = ['email', 'identification']

	def __str__(self):
		return f'{self.name} - {self.email}' 